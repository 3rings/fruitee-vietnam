import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AdminComponent } from './admin.component';

const routes: Routes = [
  {
    path: '',
    component: AdminComponent,
    children: [
      {
        path: '',
        loadChildren:()=>import('../../modules/admin/product/product.module').then(m=>m.ProductModule)
      },
      {
        path: 'product',
        loadChildren:()=>import('../../modules/admin/product/product.module').then(m=>m.ProductModule)
      },
      {
        path: 'category',
        loadChildren:()=>import('../../modules/admin/category/category.module').then(m=>m.CategoryModule)
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AdminRoutingModule { }
