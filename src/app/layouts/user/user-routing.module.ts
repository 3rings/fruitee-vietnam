import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { UserComponent } from './user.component';

const routes: Routes = [
  {
    path: '',
    component: UserComponent,
    children: [
      {
        path: '',
        loadChildren:()=>import('../../modules/user/home/home.module').then(m=>m.HomeModule)
      },
      {
        path: 'home',
        loadChildren:()=>import('../../modules/user/home/home.module').then(m=>m.HomeModule)
      },
      {
        path: 'about',
        loadChildren:()=>import('../../modules/user/about/about.module').then(m=>m.AboutModule)
      },
      {
        path: 'menu',
        loadChildren:()=>import('../../modules/user/menu/menu.module').then(m=>m.MenuModule)
      },
      {
        path: 'promotion',
        loadChildren:()=>import('../../modules/user/promotion/promotion.module').then(m=>m.PromotionModule)
      },
      {
        path: 'contact',
        loadChildren:()=>import('../../modules/user/contact/contact.module').then(m=>m.ContactModule)
      },
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class UserRoutingModule { }
