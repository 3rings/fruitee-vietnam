import { Component, OnInit, AfterViewInit } from '@angular/core';

@Component({
  selector: 'app-header-user',
  templateUrl: './header-user.component.html',
  styleUrls: ['./header-user.component.scss']
})
export class HeaderUserComponent implements AfterViewInit {

  constructor() { }

  ngOnInit(): void {
  }


  ngAfterViewInit():void {
    var open = document.querySelector('.open');
    var none = document.querySelector('.none');
    var close = document.querySelector('.closes');

    open.addEventListener('click', () => {
        none.classList.add('active');
        open.classList.remove('active');
        close.classList.add('active');
    })

    close.addEventListener('click', () => {
        none.classList.remove('active');
        open.classList.add('active');
        close.classList.remove('active');
    })
  }


}
