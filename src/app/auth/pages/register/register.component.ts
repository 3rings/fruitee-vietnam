import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit {

  formRegister: FormGroup;
  error = '';
  submitted = false;
  constructor() {
    this.formRegister = new FormGroup({
      fullName: new FormControl('', [Validators.required]),
      email: new FormControl('', [Validators.required, Validators.email]),
      password: new FormControl('', [Validators.required, Validators.minLength(6)]),
      rePassword: new FormControl('', [Validators.required, Validators.minLength(6)])
    })
   }


  ngOnInit(): void {
  }

  submitRegister(){
    const regex = /^\S*$/;
    this.submitted = true;
    if (this.formRegister.invalid) {
      if (this.formRegister.controls.fullName.value == '' && this.formRegister.controls.email.value == '' && this.formRegister.controls.password.value == '' && this.formRegister.controls.rePassword.value == '') {
        this.error = 'Vui lòng nhập đầy đủ thông tin trên!'
      } else {
        if (this.formRegister.controls.fullName.value == '') {
          this.error = 'Vui lòng nhập họ tên của bạn'
        }
        if (this.formRegister.controls.email.value == '') {
          this.error = 'Vui lòng nhập email của bạn'
        }
        if (!regex.test(this.formRegister.controls.email.value)) {
          this.error = 'Email người dùng không được chứa khoảng chắn'
        }
        if (this.formRegister.controls.password.value == '') {
          this.error = 'Vui lòng nhập password của bạn'
        }
        if (this.formRegister.controls.rePassword.value == '') {
          this.error = 'Vui lòng nhập password của bạn'
        }
        if (this.formRegister.controls.password.value != this.formRegister.controls.rePassword.value) {
          this.error = 'Vui lòng nhập password trùng với repassword'
        }
      }
      this.submitted = false;
      return false;
    }
  }
}
