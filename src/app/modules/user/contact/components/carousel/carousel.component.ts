import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-carousel',
  templateUrl: './carousel.component.html',
  styleUrls: ['./carousel.component.scss']
})
export class CarouselComponent implements OnInit {

  formContact: FormGroup;
  error = '';
  submitted = false;
  phonePattern = /^[0-9]{10,12}$/;
  constructor() {
    this.formContact = new FormGroup({
      name: new FormControl('', [Validators.required]),
      phone: new FormControl('', [Validators.required, Validators.pattern(this.phonePattern)]),
      email: new FormControl('', [Validators.required, Validators.email]),
      text: new FormControl('', [Validators.required])
    })
   }

  ngOnInit(): void {
  }

  submitContact(){
    const regex = /^\S*$/;
    this.submitted = true;
    if (this.formContact.invalid) {
      if (this.formContact.controls.name.value == '' && this.formContact.controls.phone.value == '' &&
          this.formContact.controls.email.value == '' && this.formContact.controls.text.value == '') {
        this.error = 'Vui lòng nhập đầy đủ thông tin.'
      } else {
        if (this.formContact.controls.name.value == '') {
          this.error = 'Vui lòng nhập họ và tên của bạn.'
        }
        if (this.formContact.controls.phone.value == '') {
          this.error = 'Vui lòng nhập số điện thoại của bạn.'
        }
        if (this.formContact.controls.email.value == '') {
          this.error = 'Vui lòng nhập email của bạn.'
        }
        if (!regex.test(this.formContact.controls.email.value)) {
          this.error = 'Email người dùng không được chứa khoảng trắng.'
        }
        if (this.formContact.controls.text.value == '') {
          this.error = 'Vui lòng nhập nội dung.'
        }
      }
      this.submitted = false;
      return false;
    }
    if(this.submitted == true){
      alert('Gửi Email thành công!')
    }
  }
}
