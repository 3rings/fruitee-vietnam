import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AboutRoutingModule } from './about-routing.module';
import { AboutComponent } from './about.component';
import { CarouselComponent } from './components/carousel/carousel.component';
import { IntroduceComponent } from './components/introduce/introduce.component';
import { SwiperFruiteeComponent } from './components/swiper-fruitee/swiper-fruitee.component';


import { NgxUsefulSwiperModule } from 'ngx-useful-swiper';

@NgModule({
  declarations: [
    AboutComponent,
    CarouselComponent,
    IntroduceComponent,
    SwiperFruiteeComponent
  ],
  imports: [
    CommonModule,
    AboutRoutingModule,
    NgxUsefulSwiperModule
  ]
})
export class AboutModule { }
