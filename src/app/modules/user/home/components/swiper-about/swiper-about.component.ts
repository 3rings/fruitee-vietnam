import { Component} from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { DialogAboutComponent } from './dialog-about/dialog-about.component';

@Component({
  selector: 'app-swiper-about',
  templateUrl: './swiper-about.component.html',
  styleUrls: ['./swiper-about.component.scss']
})
export class SwiperAboutComponent{

  constructor(public dialog: MatDialog) {}

  openDialog() {
    const dialogRef = this.dialog.open(DialogAboutComponent,{
      width: '800px'
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log(`Dialog result: ${result}`);
    });
  }

}
