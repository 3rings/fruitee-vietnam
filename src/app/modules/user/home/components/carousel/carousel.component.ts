import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-carousel',
  templateUrl: './carousel.component.html',
  styleUrls: ['./carousel.component.scss']
})
export class CarouselComponent implements OnInit {


  formEmail: FormGroup;
  error = '';
  submitted = false;
  constructor() {
    this.formEmail = new FormGroup({
      email: new FormControl('', [Validators.required, Validators.email])
    })
  }

  ngOnInit(): void {
  }

  submitEmail(){
    const regex = /^\S*$/;
    this.submitted = true;
    if (this.formEmail.invalid) {
        if (this.formEmail.controls.email.value == '') {
          this.error = 'Vui lòng nhập email của bạn'
        }
        else
        if (!regex.test(this.formEmail.controls.email.value)) {
          this.error = 'Email người dùng không được chứa khoảng chắn'
        }
      this.submitted = false;
      return false;
    }
    if(this.submitted == true){
      alert('Gửi Email thành công!')
    }
  }
}
