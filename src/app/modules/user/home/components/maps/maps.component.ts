import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-maps',
  templateUrl: './maps.component.html',
  styleUrls: ['./maps.component.scss']
})
export class MapsComponent implements OnInit {

  // menuLists = ['Fruitee Sư Vạn Hạnh', 'Fruitee Ba Tháng Hai', 'Fruitee Hồ Con Rùa', 'Fruitee Nguyễn Huệ']
  selectedList:any;

  menuLists = [
    {
      name: 'Fruitee Sư Vạn Hạnh',
      adress: '828/38 Sư Vạn Hạnh, P14, Q10, TP.HCM',
      time: '8h - 22h | Thứ 2 - Chủ nhật'
    },
    {
      name: 'Fruitee Ba Tháng Hai',
      adress: '606/38 Ba Tháng Hai, P14 Q10, TP.HCM',
      time: '10h - 22h | Thứ 2 - Chủ nhật'
    },
    {
      name: 'Fruitee Hồ Con Rùa',
      adress: '828/38 Sư Vạn Hạnh, P14, Q10, TP.HCM',
      time: '8h - 22h | Thứ 2 - Chủ nhật'
    },
    {
      name: 'Fruitee Nguyễn Huệ',
      adress: '828/38 Sư Vạn Hạnh, P14, Q10, TP.HCM',
      time: '8h - 22h | Thứ 2 - Chủ nhật'
    },
  ]


  constructor() { }

  ngOnInit(){

    this.selectedList = this.menuLists[0].name;

  }



  openMenuList(menuList: any){
    console.log(menuList.name)
    this.selectedList = menuList.name;
  }

}
