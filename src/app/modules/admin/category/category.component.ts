import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-category',
  templateUrl: './category.component.html',
  styleUrls: ['./category.component.scss']
})
export class CategoryComponent implements OnInit {

  public category: any[] = [
    {
      id: 1,
      name: 'Trà trái cây',
    },
    {
      id: 2,
      name: 'Trà sữa',
    },
    {
      id: 3,
      name: 'Nước ép',
    },
    {
      id: 3,
      name: 'Sinh tố',
    },
  ]
  constructor() { }

  ngOnInit(): void {
  }

}
