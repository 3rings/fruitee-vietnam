// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig: {
    apiKey: "AIzaSyD0C4OIjRBeU73ilKdwI9qWoXospZy9wnA",
    authDomain: "fruiteevietnam.firebaseapp.com",
    projectId: "fruiteevietnam",
    storageBucket: "fruiteevietnam.appspot.com",
    messagingSenderId: "883465955010",
    appId: "1:883465955010:web:4d62b107cf74f045d97d5b",
    measurementId: "G-GVTCBJK78R"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
